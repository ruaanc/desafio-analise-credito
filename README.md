# desafio-analise-credito

### About Project 
The project consists of a credit analysis api. 
Where we have users with two types of permissions, are stamps: capitation user and analyst user. 
The capture user will be responsible for registering new clients for analysis. 
And the user analyst will be responsible to perform the customer credit analysis.

### Entity Representation 

*  Entity Client: The Client entity is the object responsible for representing customer information.
*  Entity Proposal: The Proposal entity is the object responsible for representing the information of a proposal.
*  Entity UserAnalyse: The UserAnalyse entity is the object responsible for representing the analyst user information.
*  Entity UserCaptation: The UserCaptation entity is the object responsible for representing the capitation user information.

### Used Tools

*  Java 8
*  JDK 1.8
*  Maven 3
*  JUnit 4
*  PostgreSQL

  - Installation:
    ```
    * git clone https://gitlab.com/ruaanc/desafio-analise-credito.git
    * cd desafio-analise-credito
    * Import folder into any one in STS or in a preferred IDE.
    * src/main/resources copy application.properties.example for application.properties and change <database>, <username> and <password>.
    
    ```
### PATH
    
  - PATH UserCaptation:
    ```
    * /api/user/captation/{id} -> return by id (Get);
    * /api/user/captation/all -> return all (Get);
    * /api/user/captation/new -> create (Post);
    
    ```
  - PATH UserAnalyse:
    ```
    * /api/user/analyse/{id} -> return by id (Get);
    * /api/user/analyse/all -> return all (Get);
    * /api/user/analyse/new -> create (Post);
    
    ```
  - PATH Proposal:
    ```
    * /api/proposal/{id} -> return by id (Get);
    * /api/proposal/all -> return all (Get);
    * /api/proposal/new_proposal -> create (Post);
    * /api/proposal/analyze/{id_analyst}/{id_proposal}/{result} -> Perform propostal analysis (Put);
    
    ```
  - PATH Client:
    ```
    * /api/client/{id} -> return by id (Get);
    * /api/client/all -> return all (Get);
    * /api/client/pending -> return all clients pending (Get);
    * /api/client/new_client/{id_pickup} -> Create new client (Post);
    
    ```
### Example

   - Create UserCaptation:
   ```
   * Post
    {
		"username": "Julio",
		"cpf": "222.333.444 - 55"
    }
    
   ```
    ```
    * Return
    {
        "data": {
            "id": 1,
            "username": "Julio",
            "userType": "CAPITATION",
            "cpf": "222.333.444 - 55"
        },
        "errors": []
    }
    
    ```
   - Create Client:
   
    ```
    * Post: Path -> http://localhost:8081/api/client/new_client/1
    
    {
		"name": "Luiz",
		"cpf": "1234",
		"proposal": {
			"credit": 1.1
		},
		"pickup": {
			
		}
    }
    ```
    ```
    * Return
    
    {
        "data": {
            "id": 1,
            "name": "Luiz",
            "cpf": "1234",
            "proposal": {
                "id": 1,
                "credit": 1.1,
                "status": "PENDING",
                "requestDate": "2019-11-04T23:04:10.536+0000",
                "replyDate": null,
                "analyst": null
            },
            "pickup": {
                "id": 1,
                "username": "Julio",
                "userType": "CAPITATION",
                "cpf": "222.333.444 - 55"
            }
        },
        "errors": []
    }
    
    ```
   - Create UserAnalyse
   ```
    * Post
    
    {
		"username": "Ruan Carlos",
		"cpf": "111.222.333-44"
    }
   ```
   ```
   * Return
   
   {
    "data": {
        "id": 2,
        "username": "Ruan Carlos",
        "userType": "ANALYZE",
        "cpf": "111.222.333-44"
    },
        "errors": []
    }
   
   ```
   - Analyse
   ```
   * PUT: Path -> /api/proposal//analyze/2/1/APPROVED
   * Return
   
   {
    "data": {
        "id": 1,
        "credit": 1.1,
        "status": "APPROVED",
        "requestDate": "2019-11-04T23:04:10.536+0000",
        "replyDate": "2019-11-04T23:16:19.209+0000",
        "analyst": {
            "id": 2,
            "username": "Ruan Carlos",
            "userType": "ANALYZE",
            "cpf": "111.222.333-44"
        }
     },
    "errors": []
    }
   
   ```
   
   
    

    
    
    
    

