package org.creditanalysis.test.user;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.creditanalysis.user.User;
import org.creditanalysis.user.UserService;
import org.creditanalysis.user.UserType;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

	public static final String EXAMPLE_NAME = "Julio";
	
	public static final String EXAMPLE_CPF = "123.456.789-10";
	
	public static final UserType EXEMPLE_USER_TYPE = UserType.CAPITATION;
	
	@Autowired
	private UserService service;
	
	@Test
	public void t01_toChange() {
		User user =  createUser(service, EXAMPLE_NAME, EXAMPLE_CPF, EXEMPLE_USER_TYPE);
		
		service.toChange(user.getId(), UserType.ANALYZE);
		
		assertEquals(user.getUserType(), UserType.ANALYZE);
	}

	static User createUser(UserService service, String name, String cpf, UserType userType) {
		
		User user = new User(null, name, cpf);
		
		user.setType(userType);
		
		service.insert(user);
		
		return user;
	}
	
}
