package org.creditanalysis.user.analyse;

import java.util.List;

public interface UserAnalyseService {

	public UserAnalyse find(Long id);

	public List<UserAnalyse> findAll();

	public UserAnalyse insert(UserAnalyse obj);
	
}
