package org.creditanalysis.user.analyse;

import java.util.List;

import javax.validation.Valid;

import org.creditanalysis.response.Response;
import org.creditanalysis.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/user/analyse")
public class UserAnalyseController {
	
	@Autowired
	private UserAnalyseService service;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<UserAnalyse> findById(@PathVariable Long id) {
		
		UserAnalyse obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<UserAnalyse>> findAll() {
		
		List<UserAnalyse> obj = service.findAll();
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<Response<UserAnalyse>> save(@Valid @RequestBody UserAnalyse obj) {
		obj = service.insert(obj);
		
		userService.toChange(obj.getId(), UserAnalyse.getType());
		Response<UserAnalyse> response = new Response<UserAnalyse>();
		response.setData(obj);
		return ResponseEntity.ok(response);
	}

}
