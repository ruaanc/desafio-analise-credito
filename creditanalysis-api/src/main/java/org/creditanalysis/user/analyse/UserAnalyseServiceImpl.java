package org.creditanalysis.user.analyse;

import java.util.List;
import java.util.Optional;

import org.creditanalysis.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAnalyseServiceImpl implements UserAnalyseService {
	
	@Autowired
	private UserAnalyseRepository repository;
	
	@Override
	public UserAnalyse find(Long id) {
		Optional<UserAnalyse> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Object not found ! id: " + id + ", Tipo: " + UserAnalyse.class.getName()));
	}
	
	@Override
	public List<UserAnalyse> findAll() {
		
		List<UserAnalyse> users = repository.findAll();
		
		return users;
	}
	
	@Override
	public UserAnalyse insert(UserAnalyse obj) {
		obj.setId(null);
		return repository.save(obj);
	}

}
