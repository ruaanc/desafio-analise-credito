package org.creditanalysis.user.analyse;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAnalyseRepository extends JpaRepository<UserAnalyse, Long>{

}
