package org.creditanalysis.user.analyse;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.creditanalysis.user.User;
import org.creditanalysis.user.UserType;

@Entity
@Table(name = "user_analyse")
public class UserAnalyse extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static UserType TYPE = UserType.ANALYZE;

	public UserAnalyse() {
	}

	public UserAnalyse(Long id, String username, String cpf) {
		super(id, username, cpf);
	}

	public static UserType getType() {
		return UserAnalyse.TYPE;
	}
	
}
