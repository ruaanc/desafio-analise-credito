package org.creditanalysis.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repository;
	
	@Override
	public void toChange(Long id, UserType type) {
		
		User user = repository.getOne(id);
		
		user.setType(type);
		
		repository.save(user);
		
	}
	
	@Override
	public User insert(User user) {
		
		user.setId(null);
		return repository.save(user);
	}
	
}
