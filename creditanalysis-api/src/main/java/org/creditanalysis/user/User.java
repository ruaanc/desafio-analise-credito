package org.creditanalysis.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "user_entity")
@Inheritance(strategy=InheritanceType.JOINED)
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String username;
	
	@Column(name = "user_type")
	private UserType userType;
	
	@Column(name = "CPF")
	private String cpf;
	
	/* 
	 * Constructors
	 */
	public User(Long id, String username, String cpf) {
		super();
		id = this.id;
		this.username = username;
		this.cpf = cpf;
	}
	
	public User() {
	}
	
	/*
	 * Getters and Setters
	 */

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setType(UserType type) {
		this.userType = type;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
}
