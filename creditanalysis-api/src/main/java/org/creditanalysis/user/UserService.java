package org.creditanalysis.user;

public interface UserService {
	
	public void toChange(Long id, UserType type);

	public User insert(User user);
}
