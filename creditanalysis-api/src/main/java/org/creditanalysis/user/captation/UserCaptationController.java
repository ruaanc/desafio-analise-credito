package org.creditanalysis.user.captation;

import java.util.List;

import javax.validation.Valid;

import org.creditanalysis.response.Response;
import org.creditanalysis.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/user/captation")
public class UserCaptationController {
	
	@Autowired
	private UserCaptationService service;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<UserCaptation> findById(@PathVariable Long id) {
		
		UserCaptation obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<UserCaptation>> findAll() {
		
		List<UserCaptation> obj = service.findAll();
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<Response<UserCaptation>> save(@Valid @RequestBody UserCaptation obj) {
		
		obj = service.insert(obj);
		
		userService.toChange(obj.getId(), UserCaptation.getType());
		Response<UserCaptation> response = new Response<UserCaptation>();
		response.setData(obj);
		return ResponseEntity.ok(response);
	}

}
