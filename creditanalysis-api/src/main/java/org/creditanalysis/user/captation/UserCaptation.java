package org.creditanalysis.user.captation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.creditanalysis.user.User;
import org.creditanalysis.user.UserType;

@Entity
@Table(name = "user_captation")
public class UserCaptation extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Column(name = "type")
	private static UserType TYPE = UserType.CAPITATION;
	
	public UserCaptation() {
	}
	
	public UserCaptation(Long id, String username, String cpf) {
		super(id, username, cpf);
	}

	public static UserType getType() {
		return UserCaptation.TYPE;
	}
	
}
