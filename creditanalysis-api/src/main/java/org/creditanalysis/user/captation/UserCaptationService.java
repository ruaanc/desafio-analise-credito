package org.creditanalysis.user.captation;

import java.util.List;

public interface UserCaptationService {
	
	public UserCaptation find(Long id);

	public List<UserCaptation> findAll();

	public UserCaptation insert(UserCaptation obj);

}
