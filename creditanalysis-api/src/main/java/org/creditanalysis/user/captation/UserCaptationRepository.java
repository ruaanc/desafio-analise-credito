package org.creditanalysis.user.captation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCaptationRepository extends JpaRepository<UserCaptation, Long>{
	
}
