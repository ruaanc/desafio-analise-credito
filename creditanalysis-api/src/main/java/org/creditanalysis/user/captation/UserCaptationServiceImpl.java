package org.creditanalysis.user.captation;

import java.util.List;
import java.util.Optional;

import org.creditanalysis.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserCaptationServiceImpl implements UserCaptationService {
	
	@Autowired
	private UserCaptationRepository repository;
	
	@Override
	public UserCaptation find(Long id) {
		Optional<UserCaptation> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Object not found ! id: " + id + ", Tipo: " + UserCaptation.class.getName()));
	}

	@Override
	public List<UserCaptation> findAll() {
		
		List<UserCaptation> users = repository.findAll();
		
		return users;
	}

	@Override
	public UserCaptation insert(UserCaptation obj) {
		
		obj.setId(null);
		return repository.save(obj);
	}

}
