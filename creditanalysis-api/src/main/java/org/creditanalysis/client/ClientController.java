package org.creditanalysis.client;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.creditanalysis.exceptions.UnauthorizedException;
import org.creditanalysis.proposal.Status;
import org.creditanalysis.response.Response;
import org.creditanalysis.user.captation.UserCaptation;
import org.creditanalysis.user.captation.UserCaptationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/client")
public class ClientController {
	private static final Logger log = LoggerFactory.getLogger(Client.class);
	
	@Autowired
	private ClientService service;
	
	@Autowired
	private UserCaptationService pickupService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Client> findById(@PathVariable Long id) {
		
		Client obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<Client>> findAll() {
		
		List<Client> obj = service.findAll();
		return ResponseEntity.ok().body(obj);
	}
	
	/*
	 * Return all clients with their pending proposals.
	 */
	@RequestMapping(value="/pending", method=RequestMethod.GET)
	public ResponseEntity<List<Client>> findAllPending(){
		
		List<Client> obj = service.findAllPending();
		return ResponseEntity.ok().body(obj);
	}
	
	/*
	 * This endpoint has the function of registering new clients.
	 * To register you will need to register the proposal and the capitation user.
	 */
	@RequestMapping(value="/new_client/{id_pickup}", method=RequestMethod.POST)
	public ResponseEntity<Response<Client>> register_client(@PathVariable Long id_pickup, 
			@Valid @RequestBody Client obj, BindingResult result) {
		
		UserCaptation pickup; 
		
		try {
			pickup = pickupService.find(id_pickup);
		}catch(UnauthorizedException e) {
			throw new UnauthorizedException("User unauthorized");
		}
		
		service.toChange(obj, pickup, Status.PENDING, new Date());
		
		log.info("Registering client...", obj.getName());
		
		obj = service.insert(obj);
		
		Response<Client> response = new Response<Client>();
		response.setData(obj);
		return ResponseEntity.ok(response);
	}

}
