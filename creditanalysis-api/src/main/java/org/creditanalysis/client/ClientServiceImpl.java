package org.creditanalysis.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.creditanalysis.exceptions.ObjectNotFoundException;
import org.creditanalysis.proposal.Proposal;
import org.creditanalysis.proposal.Status;
import org.creditanalysis.user.captation.UserCaptation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {
	
	@Autowired
	private ClientRepository repository;
	
	@Override
	public Client find(Long id) {
		Optional<Client> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Object not found ! id: " + id + ", Tipo: " + Client.class.getName()));
	}

	@Override
	public List<Client> findAll() {
		
		List<Client> clients = repository.findAll();
		
		return clients;
	}
	
	@Override
	public List<Client> findAllPending() {
		
		List<Client> clients = this.findAll();
		List<Client> clientsPending = new ArrayList<>();
		for(Client client : clients) {
			if(client.getProposal().getStatus().equals(Status.PENDING)) {
				clientsPending.add(client);
			}
		}
		return clientsPending;
	}
	
	@Override
	public Client insert(Client obj) {
		
		obj.setId(null);
		return repository.save(obj);
	}
	
	@Override
	public Client create(String name, String cpf, Proposal proposal, UserCaptation pickup) {
		return new Client(name, cpf, proposal, pickup);
	}
	
	@Override
	public void toChange(Client client, UserCaptation pickup, Status status, Date date) {
		client.setPickup(pickup);
		client.getProposal().setStatus(Status.PENDING);
		client.getProposal().setRequestDate(new Date());
	}

}
