package org.creditanalysis.client;

import java.util.Date;
import java.util.List;

import org.creditanalysis.proposal.Proposal;
import org.creditanalysis.proposal.Status;
import org.creditanalysis.user.captation.UserCaptation;

public interface ClientService {

	public Client find(Long id);

	public List<Client> findAll();
	
	public List<Client> findAllPending();

	public Client insert(Client obj);
	
	public Client create(String name, String cpf, Proposal proposal, UserCaptation pickup);
	
	public void toChange(Client client, UserCaptation pickup, Status status, Date date);
	

}
