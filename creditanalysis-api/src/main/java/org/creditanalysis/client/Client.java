package org.creditanalysis.client;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.creditanalysis.proposal.Proposal;
import org.creditanalysis.user.captation.UserCaptation;

@Entity
@Table(name = "client_entity")
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "CPF ")
	private String cpf;
	
	@OneToOne(targetEntity = Proposal.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "proposal_id")
	private Proposal proposal;
	
	@OneToOne(targetEntity = UserCaptation.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "pickup_id")
	private UserCaptation pickup;
	
	/* 
	 * Constructors
	 */
	public Client(String name, String cpf, Proposal proposal, UserCaptation pickup) {
		super();
		this.name = name;
		this.cpf = cpf;
		this.proposal = proposal;
		this.pickup = pickup;
	}

	public Client() {
	}
	
	/*
	 * Getters and Setters
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Proposal getProposal() {
		return proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

	public UserCaptation getPickup() {
		return pickup;
	}

	public void setPickup(UserCaptation pickup) {
		this.pickup = pickup;
	}
	

}
