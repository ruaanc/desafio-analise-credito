package org.creditanalysis.proposal;

import java.util.List;

import javax.validation.Valid;

import org.creditanalysis.exceptions.UnauthorizedException;
import org.creditanalysis.response.Response;
import org.creditanalysis.user.analyse.UserAnalyse;
import org.creditanalysis.user.analyse.UserAnalyseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/proposal")
public class ProposalController {
	
	@Autowired
	private ProposalService service;
	
	@Autowired
	private UserAnalyseService analystService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Proposal> findById(@PathVariable Long id) {
		
		Proposal obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<Proposal>> findAll() {
		
		List<Proposal> obj = service.findAll();
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/new_proposal", method=RequestMethod.POST)
	public ResponseEntity<Response<Proposal>> save(@Valid @RequestBody Proposal obj) {
		obj = service.insert(obj);
		Response<Proposal> response = new Response<Proposal>();
		response.setData(obj);
		return ResponseEntity.ok(response);
	}
	
	@RequestMapping(value="/analyze/{id_analyst}/{id_proposal}/{result}", method=RequestMethod.PUT)
	public ResponseEntity<Response<Proposal>> analyzeProposal(@PathVariable Long id_analyst, 
			@PathVariable Long id_proposal, @PathVariable Status result) {
		
		UserAnalyse analyst;
		try {
			analyst = analystService.find(id_analyst);
		}catch(UnauthorizedException e) {
			throw new UnauthorizedException("User unauthorized");
		}
		
		Proposal proposal = service.find(id_proposal);
		
		Proposal obj = service.analyzeProposal(analyst, proposal, result);
		Response<Proposal> response = new Response<Proposal>();
		response.setData(obj);
		return ResponseEntity.ok(response);
	}

}
