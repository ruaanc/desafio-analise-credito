package org.creditanalysis.proposal;

import java.util.Date;
import java.util.List;

import org.creditanalysis.user.analyse.UserAnalyse;

public interface ProposalService {

	public Proposal find(Long id);

	public List<Proposal> findAll();

	public Proposal insert(Proposal obj);
	
	public Proposal create(Double credit, Status status, Date requestDate, Date replyDate, UserAnalyse analyst);
	
	public Proposal analyzeProposal(UserAnalyse analyst, Proposal proposal, Status status);

}
