package org.creditanalysis.proposal;

public enum Status {
	
	APPROVED,
	DISAPPROVED,
	PENDING

}
