package org.creditanalysis.proposal;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.creditanalysis.exceptions.ObjectNotFoundException;
import org.creditanalysis.user.analyse.UserAnalyse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProposalServiceImpl implements ProposalService {
	
	@Autowired
	private ProposalRepository repository;
	
	@Override
	public Proposal find(Long id) {
		Optional<Proposal> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Object not found ! id: " + id + ", Tipo: " + Proposal.class.getName()));
	}
	
	@Override
	public List<Proposal> findAll() {
		
		List<Proposal> proposals = repository.findAll();
		
		return proposals;
	}
	
	@Override
	public Proposal insert(Proposal obj) {
		obj.setId(null);
		obj.setRequestDate(new Date());
		return repository.save(obj);
	}
	
	@Override
	public Proposal create(Double credit, Status status, Date requestDate, Date replyDate, UserAnalyse analyst) {
		
		return new Proposal(credit, status, requestDate, replyDate, analyst);
		
	}
	
	@Override
	public Proposal analyzeProposal(UserAnalyse analyst, Proposal proposal, Status status) {
		
		
		proposal.setAnalyst(analyst);
		proposal.setStatus(status);
		proposal.setReplyDate(new Date());
		
		return repository.save(proposal);
	}

}
