package org.creditanalysis.proposal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.creditanalysis.user.analyse.UserAnalyse;


@Entity
@Table(name = "proposal_entity")
public class Proposal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "credit")
	private Double credit;
	
	@Column(name = "status")
	private Status status;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@Column(name = "reply_date")
	private Date replyDate;
	
	@OneToOne(targetEntity = UserAnalyse.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "analyst_id")
	private UserAnalyse analyst;
	
	
	/* 
	 * Constructors
	 */
	public Proposal(Double credit, Status status, Date requestDate, Date replyDate, UserAnalyse analyst) {
		super();
		this.credit = credit;
		this.status = status;
		this.requestDate = requestDate;
		this.replyDate = replyDate;
		this.analyst = analyst;
	}

	public Proposal() {
	}
	
	/*
	 * Getters and Setters
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCredit() {
		return credit;
	}

	public void setCredit(Double credit) {
		this.credit = credit;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public UserAnalyse getAnalyst() {
		return analyst;
	}

	public void setAnalyst(UserAnalyse analyst) {
		this.analyst = analyst;
	}
	
	
}
